FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y openssh-client vim apt-transport-https ca-certificates curl software-properties-common openjdk-8-jdk locales bzip2 zip wget ffmpeg && \
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
    apt-key fingerprint 0EBFCD88 && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && \
    add-apt-repository -y ppa:ansible/ansible && \
    curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    add-apt-repository ppa:cwchien/gradle &&\
    apt-get update && \
    apt-get install -y nodejs docker-ce yarn ansible gradle build-essential && \
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    apt-get -y install fonts-liberation libappindicator3-1 libatk-bridge2.0-0 libgtk-3-0 libxss1 xdg-utils && \
    dpkg -i google-chrome*.deb && \
    rm -rf google-chrome*.deb && \
    rm -rf /var/lib/apt/lists/*

RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG=en_US.UTF-8
